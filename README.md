It is a test project that allows you to manipulate users (CRUD).

The project built upon:
- React
- Redux
- Redux-Thunk
- React Router
- Material-UI

The full description can be found in the current repo with the name 'Users Application - Home Exercise.pdf'.

The project is deployed here - https://test-task-users.netlify.com/.

In the project directory, you can run:
- 'npm start' - will run the app in development mode.
- 'npm run build' - will build the app for production and put files into 'build' folder.

Use VS Code plugin (https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) to format code accordingly.
