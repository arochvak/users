// this object simulates requests to the server,
// should be modified if here will be BE server
export default {
  users: new Map(),
  _createResponseObj(r, status = 200) {
    // simulates requests with timeout for 1 second
    return new Promise(resolve => {
      setTimeout(() => resolve({ data: r, status }), 400);
    });
  },
  getUsers() {
    return this._createResponseObj(
      Array.from(this.users.values(), u => ({
        id: u.id,
        name: u.name,
        picture: u.picture,
        email: u.email,
      }))
    );
  },
  _setUserToMap(user) {
    this.users.set(user.id, user);
    return this._createResponseObj(user);
  },
  createUser(user) {
    user.id = Date.now();
    return this._setUserToMap(user);
  },
  editUser(user) {
    return this._setUserToMap(user);
  },
  deleteUser(id) {
    return this._createResponseObj(this.users.delete(id));
  },
  getUserDetails(id) {
    return this._createResponseObj(this.users.get(id));
  },
};
