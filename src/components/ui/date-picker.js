import React from 'react';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';

export default class DatePicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: props.value || '' };
    this.onChange = this.onChange.bind(this);
  }

  onChange(event) {
    const { onChange } = this.props;
    this.setState({ value: event.target.value });
    if (onChange) {
      onChange(event);
    }
  }

  render() {
    const props = this.props;
    return (
      // TODO : probably onChange in current implementation should be removed and inputRef
      // should be used. But currently can't use refs for some reason. Should be investigated in future.
      <FormControl className={props.className}>
        <InputLabel htmlFor="date-picker" shrink={true}>
          Name
        </InputLabel>
        <Input
          {...props}
          id="date-picker"
          type="date"
          onChange={this.onChange}
          value={this.state.value}
        />
      </FormControl>
    );
  }
}
