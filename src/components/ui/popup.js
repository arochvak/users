import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

export default ({ open, onClose, description, title = '', actions }) => (
  <Dialog
    open={open}
    keepMounted
    onClose={onClose}
    aria-labelledby="dialog__title"
  >
    <DialogTitle id="dialog__title">{title}</DialogTitle>
    <DialogContent>{description}</DialogContent>
    <DialogActions>{actions}</DialogActions>
  </Dialog>
);
