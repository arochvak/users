import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

export default ({ className, picture, header, message, actions }) => (
  <Card className={className}>
    <CardMedia className={`${className}__image`} image={picture} />
    <CardContent className={`${className}__content`}>
      <Typography gutterBottom variant="h5" component="h2">
        {header}
      </Typography>
      <Typography variant="body2" color="textSecondary" component="div">
        {message}
      </Typography>
    </CardContent>
    <CardActions>{actions}</CardActions>
  </Card>
);
