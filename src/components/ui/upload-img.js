import React from 'react';
import Button from './button';
import './upload-img.css';

export default class UploadImg extends React.Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(event) {
    const { onChange } = this.props;
    // WARNING: the code is not tested in different browsers,
    // so here could be limited support for some browsers
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.onloadend = () => {
      onChange(reader.result);
    };

    if (file) {
      reader.readAsDataURL(file); //reads the data as an URL
    } else {
      onChange('');
    }
  }

  render() {
    return (
      <div className="upload">
        <input
          accept="image/*"
          className="upload__input"
          id="upload-input" // currently supports only one 'upload' button on the page,
          // can be fixed by accepting it from 'props'
          multiple
          type="file"
          onChange={this.onChange}
        />
        <label htmlFor="upload-input">
          <Button component="span" className="upload__button">
            {this.props.label}
          </Button>
        </label>
      </div>
    );
  }
}
