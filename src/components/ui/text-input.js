import React from 'react';
import TextField from '@material-ui/core/TextField';

export default class TextInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: props.value || '' };
    this.onChange = this.onChange.bind(this);
  }

  onChange(event) {
    const { onChange } = this.props;
    this.setState({ value: event.target.value });
    if (onChange) {
      onChange(event);
    }
  }

  render() {
    const props = this.props;
    return (
      // TODO : probably onChange incurrent implementation should be removed and inputRef
      // should be used. But currently can't use refs for some reason. Should be investigated in future.
      <TextField {...props} onChange={this.onChange} value={this.state.value} />
    );
  }
}
