import React from 'react';
import Button from '../ui/button';
import Loader from '../ui/loader';
import ModifyForm from './modify-form';
import { ROUTES } from '../../constants/routes';
import { DEFAULT_IMAGE } from '../../constants';
import './modify-user-view.css';

class ModifyUserView extends React.Component {
  currentId = '';
  constructor(props) {
    super(props);
    this.currentId = props.match.params.id
      ? Number.parseInt(props.match.params.id)
      : '';
    this.state = { pic: DEFAULT_IMAGE };
    this.goToGrid = this.goToGrid.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    if (this.currentId) {
      this.props.getUser(this.currentId);
    }
  }

  static getDerivedStateFromProps(props, state) {
    const currentId = props.match.params.id
      ? Number.parseInt(props.match.params.id)
      : '';
    if (
      currentId &&
      currentId === props.userDetails.response.id &&
      state.pic === DEFAULT_IMAGE
    ) {
      // updates image in state only for first rerender, when we received image from BE
      return { pic: props.userDetails.response.picture };
    }
    return null;
  }

  onDelete() {
    this.props.deleteUser(this.currentId);
    this.goToGrid();
  }

  onSubmit(user) {
    const { updateUser, createUser } = this.props;
    const isInEditMode = !!this.currentId;
    if (isInEditMode) {
      updateUser(user);
    } else {
      createUser(user);
    }
    this.goToGrid();
  }

  goToGrid() {
    this.props.history.push(ROUTES.GRID_VIEW);
  }

  render() {
    const isInEditMode = !!this.currentId;
    let {
      userDetails: { response: userData },
    } = this.props;
    if (this.currentId !== userData.id) {
      if (isInEditMode) {
        return (
          <div className="modify-user-loader">
            <Loader />
          </div>
        );
      }
      userData = {};
    }
    return (
      <div className="modify-user">
        <div className="modify-user__header">
          <Button onClick={this.goToGrid}>Back</Button>
        </div>
        <ModifyForm
          userData={userData}
          isInEditMode={isInEditMode}
          onSubmit={this.onSubmit}
          onDelete={this.onDelete}
          pic={this.state.pic}
        />
      </div>
    );
  }
}

export default ModifyUserView;
