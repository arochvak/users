import React from 'react';
import Button from '../ui/button';
import Input from '../ui/text-input';
import DatePicker from '../ui/date-picker';
import Uploader from '../ui/upload-img';

const userFields = {
  name: 'name',
  email: 'email',
  picture: 'picture',
  birthday: 'birthday',
  address: 'address',
};
export default class ModifyForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { pic: props.pic };
    this.onSubmit = this.onSubmit.bind(this);
    this.onNameChanged = this.onFormFieldChanged.bind(this, userFields.name);
    this.onBirthdayChanged = this.onFormFieldChanged.bind(
      this,
      userFields.birthday
    );
    this.onAddressChanged = this.onFormFieldChanged.bind(
      this,
      userFields.address
    );
    this.onEmailChanged = this.onFormFieldChanged.bind(this, userFields.email);
    this.onImgUploaded = this.onImgUploaded.bind(this);
  }

  getModifiedUser() {
    const { userData } = this.props;
    return {
      id: userData.id,
      picture: this.state.pic,
      name: this[userFields.name] || userData[userFields.name],
      birthday: this[userFields.birthday] || userData[userFields.birthday],
      address: this[userFields.address] || userData[userFields.address],
      email: this[userFields.email] || userData[userFields.email],
    };
  }

  onSubmit() {
    const user = this.getModifiedUser();
    this.props.onSubmit(user);
  }

  onFormFieldChanged(name, event) {
    this[name] = event.target.value;
  }

  onImgUploaded(pic) {
    this.setState({ pic });
  }

  render() {
    const { isInEditMode, userData, onDelete } = this.props;
    const submitBtnCapture = isInEditMode ? 'Update' : 'Create';
    return (
      <>
        <div className="modify-user__form">
          {/* TODO : add validation for form fields */}
          <div className="modify-user__image-preview">
            <img src={this.state.pic} alt="user" />
          </div>
          <div className="modify-user__uploader">
            <Uploader label="Upload" onChange={this.onImgUploaded} />
          </div>
          <div className="modify-user__name">
            <Input
              value={userData.name}
              label="Full Name"
              onChange={this.onNameChanged}
            />
          </div>
          <div className="modify-user__date">
            <DatePicker
              value={userData.birthday}
              label="Birthday"
              onChange={this.onBirthdayChanged}
            />
          </div>
          <div className="modify-user__address">
            <Input
              value={userData.address}
              label="Address"
              onChange={this.onAddressChanged}
            />
          </div>
          <div className="modify-user__email">
            <Input
              value={userData.email}
              label="Email"
              onChange={this.onEmailChanged}
            />
          </div>
        </div>
        <div className="modify-user__form-buttons">
          {isInEditMode && <Button onClick={onDelete}>Delete</Button>}
          <Button onClick={this.onSubmit}>{submitBtnCapture}</Button>
        </div>
      </>
    );
  }
}
