import { connect } from 'react-redux';
import ModifyUserView from './modify-user-view';
import {
  createUser,
  updateUser,
  deleteUser,
  getUserDetails,
} from '../../store/actions/user-details';

const mapStateToProps = ({ userDetails }) => ({ userDetails });
const mapDispatchToProps = dispatch => ({
  getUser: id => dispatch(getUserDetails(id)),
  createUser: user => dispatch(createUser(user)),
  updateUser: user => dispatch(updateUser(user)),
  deleteUser: id => dispatch(deleteUser(id)),
});

const ModifyUserViewContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ModifyUserView);
export default ModifyUserViewContainer;
