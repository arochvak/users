import React from 'react';
import UserPreview from './user-preview';
import { POSSIBLE_STATUSES, POSSIBLE_STATES } from '../../constants/state';
import UserView from './user-view';
import GridHeader from './header';
import Loader from '../ui/loader';
import './grid-view.css';

class GridView extends React.Component {
  constructor(props) {
    super(props);
    this.state = { open: false };
    this.onOpen = this.onOpen.bind(this);
    this.onClose = this.onClose.bind(this);
  }

  componentDidMount() {
    this.props.getUsers();
  }

  updateGridData() {
    const { state, response } = this.props.userDetails;
    switch (state) {
      case POSSIBLE_STATES.SUCCESS:
        this.props.getUsers();
        break;
      case POSSIBLE_STATES.FAIL:
        this.setState({ errorMessage: response });
        break;
    }
  }

  componentDidUpdate(prefProps) {
    const { userDetails } = this.props;
    if (
      prefProps.userDetails.state !== userDetails.state &&
      userDetails.status === POSSIBLE_STATUSES.USER_CREATED
    ) {
      this.updateGridData();
    }
  }

  onOpen(id) {
    this.setState({ open: true });
    this.props.getUser(id);
  }

  onClose() {
    this.setState({ open: false });
  }

  getSelectedUser() {
    const { userDetails } = this.props;
    if (userDetails.status === POSSIBLE_STATUSES.GOT_USER) {
      return userDetails.response;
    }
    return {};
  }

  getClassForLoaderContainer(users, userDetails) {
    return users.state === POSSIBLE_STATES.SUCCESS &&
      userDetails.state !== POSSIBLE_STATES.PENDING
      ? 'hidden'
      : 'loading';
  }

  render() {
    const { users, userDetails, history, createUser } = this.props;
    let userData = this.getSelectedUser();
    const classForLoaderContainer = this.getClassForLoaderContainer(
      users,
      userDetails.state
    );
    return (
      <div className="grid">
        <GridHeader history={history} createUser={createUser} />
        <div className="grid__content">
          {users.response.map(item => (
            <UserPreview
              history={history}
              onOpen={this.onOpen}
              key={item.id}
              {...item}
            />
          ))}
        </div>
        <UserView
          isOpen={this.state.open}
          onClose={this.onClose}
          userData={userData}
        />
        <div className={`grid__loader ${classForLoaderContainer}`}>
          <Loader />
        </div>
      </div>
    );
  }
}

export default GridView;
