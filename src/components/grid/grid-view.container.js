import { connect } from 'react-redux';
import GridView from './grid-view';
import { createUser, getUserDetails } from '../../store/actions/user-details';
import { getUsers } from '../../store/actions/users';

const mapStateToProps = ({ users, userDetails }) => ({ users, userDetails });
const mapDispatchToProps = dispatch => ({
  getUsers: () => dispatch(getUsers()),
  createUser: user => dispatch(createUser(user)),
  getUser: id => dispatch(getUserDetails(id)),
});

const GridViewContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(GridView);
export default GridViewContainer;
