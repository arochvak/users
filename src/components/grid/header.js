import React from 'react';
import UserUtils from '../../utils/user';
import { ROUTES } from '../../constants/routes';
import Button from '../ui/button';

export default class GridHeader extends React.Component {
  constructor(props) {
    super(props);
    this.createRandomUser = this.createRandomUser.bind(this);
    this.goToCreatePage = this.goToCreatePage.bind(this);
  }

  createRandomUser() {
    this.props.createUser(UserUtils.generateRandomUser());
  }

  goToCreatePage() {
    this.props.history.push(ROUTES.getUserLink());
  }

  render() {
    return (
      <div className="grid__header">
        <Button onClick={this.goToCreatePage}>New</Button>
        <Button onClick={this.createRandomUser}>Random</Button>
      </div>
    );
  }
}
