import React from 'react';
import Card from '../ui/card';
import Button from '../ui/button';
import { ROUTES } from '../../constants/routes';
import './user-preview.css';

class UserPreview extends React.Component {
  constructor(props) {
    super(props);
    this.state = { open: false };
    this.onEdit = this.onEdit.bind(this);
    this.onOpen = this.onOpen.bind(this);
  }

  onEdit() {
    const { history, id } = this.props;
    history.push(ROUTES.getUserLink(id));
  }

  onOpen() {
    const { onOpen, id } = this.props;
    onOpen(id);
  }

  render() {
    const { picture, name, email } = this.props;
    return (
      <Card
        className="preview"
        picture={picture}
        header={name}
        message={email}
        actions={[
          <Button key="edit" onClick={this.onEdit}>
            Edit
          </Button>,
          <Button key="open" onClick={this.onOpen}>
            Open
          </Button>,
        ]}
      />
    );
  }
}

export default UserPreview;
