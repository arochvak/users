import React from 'react';
import Popup from '../ui/popup';
import Loader from '../ui/loader';
import Button from '../ui/button';
import Card from '../ui/card';

export default ({ isOpen, onClose, userData }) => (
  <Popup
    open={isOpen}
    onClose={onClose}
    description={
      userData.name ? (
        <Card
          className="view"
          picture={userData.picture}
          header={`Full Name: ${userData.name}`}
          message={
            <div className="view__message">
              <div>Birthday: {userData.birthday}</div>
              <div>Email: {userData.email}</div>
              <div>Address: {userData.address}</div>
            </div>
          }
        />
      ) : (
        <div className="popup">
          <Loader />
        </div>
      )
    }
    actions={<Button onClick={onClose}>Close</Button>}
  />
);
