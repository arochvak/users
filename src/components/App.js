import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';
import { ROUTES } from '../constants/routes';
import GridViewContainer from './grid/grid-view.container';
import ModifyUserView from './modify-user/modify-user-view.container';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path={ROUTES.GRID_VIEW} component={GridViewContainer} />
        <Route
          exact
          path={ROUTES.MODIFY_USER_VIEW}
          component={ModifyUserView}
        />
        <Redirect path="*" to={ROUTES.GRID_VIEW} />
      </Switch>
    </Router>
  );
}

export default App;
