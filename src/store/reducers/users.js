import { ACTION_TYPES, POSSIBLE_STATES } from '../../constants/state';

const defaultState = { state: '', response: [] };
const users = (state = defaultState, action) => {
  switch (action.type) {
    case ACTION_TYPES.GET_USERS_PENDING:
      return {
        state: POSSIBLE_STATES.PENDING,
        status: action.status,
        response: [],
      };
    case ACTION_TYPES.GET_USERS_SUCCESS:
      return {
        state: POSSIBLE_STATES.SUCCESS,
        status: action.status,
        response: action.response,
      };
    case ACTION_TYPES.GET_USERS_FAIL:
      return {
        state: POSSIBLE_STATES.FAIL,
        status: action.status,
        response: action.error,
      };
    default:
      return state;
  }
};

export default users;
