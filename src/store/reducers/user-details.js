import { ACTION_TYPES, POSSIBLE_STATES } from '../../constants/state';

const defaultState = { state: '', response: [] };
const userDetails = (state = defaultState, action) => {
  switch (action.type) {
    case ACTION_TYPES.GET_USER_DETAILS_PENDING:
    case ACTION_TYPES.CREATE_USER_PENDING:
    case ACTION_TYPES.UPDATE_USER_PENDING:
    case ACTION_TYPES.DELETE_USER_PENDING:
      return {
        state: POSSIBLE_STATES.PENDING,
        status: action.status,
        response: '',
      };
    case ACTION_TYPES.GET_USER_DETAILS_SUCCESS:
    case ACTION_TYPES.CREATE_USER_SUCCESS:
    case ACTION_TYPES.UPDATE_USER_SUCCESS:
    case ACTION_TYPES.DELETE_USER_SUCCESS:
      return {
        state: POSSIBLE_STATES.SUCCESS,
        status: action.status,
        response: action.response,
      };
    case ACTION_TYPES.GET_USER_DETAILS_FAIL:
    case ACTION_TYPES.CREATE_USER_FAIL:
    case ACTION_TYPES.UPDATE_USER_FAIL:
    case ACTION_TYPES.DELETE_USER_FAIL:
      return {
        state: POSSIBLE_STATES.FAIL,
        status: action.status,
        response: action.error,
      };
    default:
      return state;
  }
};

export default userDetails;
