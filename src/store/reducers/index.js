import { combineReducers } from 'redux';
import users from './users';
import userDetails from './user-details';

export default combineReducers({ users, userDetails });
