import UserApi from '../../api/user';
import { ACTION_TYPES } from '../../constants/state';

export const getUsers = () => dispatch => {
  dispatch({
    type: ACTION_TYPES.GET_USERS_PENDING,
  });
  UserApi.getUsers()
    .then(r =>
      dispatch({
        type: ACTION_TYPES.GET_USERS_SUCCESS,
        response: r.data,
        status: r.status,
      })
    )
    .catch(e =>
      dispatch({
        type: ACTION_TYPES.GET_USERS_FAIL,
        error: e.message,
        status: e.status,
      })
    );
};
