import UserApi from '../../api/user';
import { ACTION_TYPES, POSSIBLE_STATUSES } from '../../constants/state';

export const createUser = user => dispatch => {
  dispatch({
    type: ACTION_TYPES.CREATE_USER_PENDING,
  });
  UserApi.createUser(user)
    .then(r =>
      dispatch({
        type: ACTION_TYPES.CREATE_USER_SUCCESS,
        response: r.data,
        status: POSSIBLE_STATUSES.USER_CREATED,
      })
    )
    .catch(e =>
      dispatch({
        type: ACTION_TYPES.CREATE_USER_FAIL,
        error: e.message,
        status: POSSIBLE_STATUSES.USER_CREATION_FAILED, // e.status can help with more accurate message in future
      })
    );
};

export const updateUser = user => dispatch => {
  dispatch({
    type: ACTION_TYPES.UPDATE_USER_PENDING,
  });
  UserApi.editUser(user)
    .then(r =>
      dispatch({
        type: ACTION_TYPES.UPDATE_USER_SUCCESS,
        response: r.data,
        status: POSSIBLE_STATUSES.USER_UPDATED,
      })
    )
    .catch(e =>
      dispatch({
        type: ACTION_TYPES.UPDATE_USER_FAIL,
        error: e.message,
        status: POSSIBLE_STATUSES.USER_UPDATE_FAILED, // e.status can help with more accurate message in future
      })
    );
};

export const deleteUser = id => dispatch => {
  dispatch({
    type: ACTION_TYPES.DELETE_USER_PENDING,
  });
  UserApi.deleteUser(id)
    .then(r =>
      dispatch({
        type: ACTION_TYPES.DELETE_USER_SUCCESS,
        response: r.data,
        status: POSSIBLE_STATUSES.USER_DELETED,
      })
    )
    .catch(e =>
      dispatch({
        type: ACTION_TYPES.DELETE_USER_FAIL,
        error: e.message,
        status: POSSIBLE_STATUSES.USER_DELETE_FAILED, // e.status can help with more accurate message in future
      })
    );
};

export const getUserDetails = user => dispatch => {
  dispatch({
    type: ACTION_TYPES.GET_USER_DETAILS_PENDING,
  });
  UserApi.getUserDetails(user)
    .then(r =>
      dispatch({
        type: ACTION_TYPES.GET_USER_DETAILS_SUCCESS,
        response: r.data,
        status: POSSIBLE_STATUSES.GOT_USER,
      })
    )
    .catch(e =>
      dispatch({
        type: ACTION_TYPES.GET_USER_DETAILS_FAIL,
        error: e.message,
        status: POSSIBLE_STATUSES.GET_USER_FAILED, // e.status can help with more accurate message in future
      })
    );
};
