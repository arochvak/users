export const ROUTES = {
  GRID_VIEW: '/',
  MODIFY_USER_VIEW: '/user/:id?',
  getUserLink: (id = '') => `/user/${id}`,
};
