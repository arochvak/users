import { DEFAULT_IMAGE } from '../constants/index';
export default {
  generateRandomUser() {
    return {
      email: this.getRandomEmail(),
      name: this.getRandomName(),
      birthday: this.getRandomBirthday(),
      address: this.getRandomAddress(),
      picture: this.getRandomPicture(),
    };
  },
  getRandomEmail() {
    return `email${Date.now()}@gmail.com`;
  },
  getRandomName() {
    return `name ${Date.now()}`;
  },
  getRandomBirthday() {
    return '2019-08-06';
  },
  getRandomAddress() {
    return `address ${Date.now()}`;
  },
  getRandomPicture() {
    return DEFAULT_IMAGE;
  },
};
